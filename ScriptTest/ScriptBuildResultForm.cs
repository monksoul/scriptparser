﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ScriptTest
{
    public partial class ScriptBuildResultForm : Form
    {
        public ScriptBuildResultForm(string script, string result)
        {
            InitializeComponent();
            txtScript.Text = script;
            txtResult.Text = result;
        }

    }
}
